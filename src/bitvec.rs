use std::iter::Iterator;
use std::iter::IntoIterator;
use std::error::Error;
use std::cmp::min;
use std::fmt;

pub type BitVecResult<T> = Result<T, BitVecErr>;

impl BitVec {
    pub fn new() -> Self {
        BitVec {
            data : vec![0;128],
            top: 0,
            bot: 0,
        }
    }

    pub fn with_size(size: usize) -> Self{
        let len = (size+7)/8;
        let mut ret = BitVec {
            data : vec![0;len],
            top: size-1,
            bot: 0,
        };
        ret.resize(size);
        ret
    }

    pub fn len(&self) -> usize {
        self.top - self.bot
    }

    /// push a bit to the back of the bitvec
    pub fn push(&mut self, val : bool) {
        self.top += 1;
        if self.top/8 > self.data.len()-1 {
            self.resize(self.data.len()*8 + 128);
        }
        self.set(self.top-1, val);
    }

    /// returns the msb, removing that bit from the back of the bitvec
    pub fn pop(&mut self) -> BitVecResult<bool> {
        let ret = self.msb();
        if ret.is_ok() {
            self.top -= 1;
        }
        ret
    }

    /// returns the lsb, removing it from the front of the bitvec
    pub fn pop_front(&mut self) -> BitVecResult<bool> {
        let ret = self.lsb();
        if ret.is_ok() {
            self.bot += 1;
        }
        ret
    }

    pub fn at(&self, pos : usize) -> BitVecResult<bool> {
        if let Some(err) = self.check_position(pos) {
            return Err(err);
        } else {
            return Ok((self.at_raw(pos)) != 0);
        }
    }

    // idk, return error type?
    pub fn set(&mut self, pos : usize, val : bool) -> BitVecResult<()> {
        if let Some(err) = self.check_position(pos) {
            return Err(err);
        } else {
            let mut byte = self.data[pos/8];
            if val {
                byte = byte | (1u8 << pos%8)
            } else {
                byte = byte & !((1u8 as u8) << pos%8)
            }
            self.data[pos/8] = byte;

            return Ok(());
        }
    }

    /// get most significant bit (aka top bit of vec)
    pub fn msb(&self) -> BitVecResult<bool> {
        if self.top == self.bot {
            return Err(BitVecErr::EmptyVec);
        } else {
            return Ok((self.at_raw(self.top - 1) != 0));
        }
    }

    /// get least significant bit (aka bottom bit of vec)
    pub fn lsb(&self) -> BitVecResult<bool> {
        if self.top == self.bot {
            return Err(BitVecErr::EmptyVec);
        } else {
            return Ok((self.at_raw(self.bot) != 0));
        }
    }

    /// resize to support size number of bits
    fn resize(&mut self, size: usize) {
        let new_len = (size+7)/8;

        // slice of interest
        let mut i = self.bot/8;
        let shift = self.bot%8;
        let end = min(self.len()/8, new_len);
        while i < end {
            self.data[i] = (self.data[i] << shift);
            if i+1 < self.data.len() {
                self.data[i] |= (self.data[i+1] << shift);
            }
            i += 1;
        }
        self.data.resize(new_len, 0);

        self.top = self.top - self.bot;
        self.bot = 0;
    }

    fn check_position(&self, pos : usize) -> Option<BitVecErr> {
        if pos >= self.top || pos < self.bot {
            return Some(BitVecErr::IndexOutOfBounds);
        } else if self.top == self.bot {
            return Some(BitVecErr::EmptyVec);
        } else {
            return None
        }
    }

    fn at_raw(&self, pos : usize) -> u8 {
        self.data[pos/8] & (1<<(pos%8))
    }
}

pub struct BitVec {
    data : Vec<u8>,
    top : usize,
    bot : usize
}

#[derive(Debug)]
#[derive(PartialEq)]
pub enum BitVecErr {
    EmptyVec,
    IndexOutOfBounds
}

impl Error for BitVecErr {}

impl fmt::Display for BitVecErr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}
