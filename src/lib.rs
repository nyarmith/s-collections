// top-level export our modules
pub mod bitvec;

pub fn add(left: usize, right: usize) -> usize {
    left + right
}

#[cfg(test)]
mod tests {
    use super::*;
    use bitvec::{BitVec,BitVecErr};

    #[test]
    fn basic_bitvec_test() {
        let mut bv = BitVec::new();
        assert_eq!(bv.len(), 0);
        bv.push(true);
        bv.push(true);
        bv.push(false);
        assert_eq!(bv.len(), 3);
        assert_eq!(bv.at(2).unwrap(), false, "Top at was not false");
        assert_eq!(bv.at(1).unwrap(), true, "Mid at was not true",);
        assert_eq!(bv.at(0).unwrap(), true, "First at was not true",);

        // pop also tests msb
        assert_eq!(bv.pop().unwrap(), false, "Top push was not equal to false");
        assert_eq!(bv.pop().unwrap(), true, "Mid push was not equal to true",);
        assert_eq!(bv.pop().unwrap(), true, "First push was not equal to true",);
        assert_eq!(bv.len(), 0);

        // test resize
        let vals = [true,true,true,true,false,true,false,false,true];
        let mut bv = BitVec::with_size(1);
        for v in &vals {
            bv.push(*v);
        }

        // first pop_front test
        for i in 0..vals.len() {
            assert_eq!(bv.at(i).unwrap(), vals[i]);
            assert_eq!(bv.lsb().unwrap(), vals[i]);
            assert_eq!(bv.pop_front().unwrap(), vals[i]);
        }

        let empty_result = bv.pop();
        assert!(empty_result.is_err());
        assert_eq!(empty_result.expect_err("getting empty error"), BitVecErr::EmptyVec);
        assert_eq!(bv.at(3).expect_err("testing out of bounds err, but got result instead"), BitVecErr::IndexOutOfBounds);
    }
}
